
#import "JASAppDelegate.h"
#import "JASGeneticAlgo.h"

@interface JASAppDelegate() <JASGeneticAlgoDelegate>

@property (nonatomic, strong) NSString *console;
@end

@implementation JASAppDelegate

@synthesize window = _window;
@synthesize textView = _textView;
@synthesize textField = _textField;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
}

- (IBAction)handleRunAlgorithmButton:(id)sender 
{
    [self.textView setString:@"Processing..."];
    [self performSelectorInBackground:@selector(runAlgorithm) withObject:nil];
}

- (void)runAlgorithm
{
    self.console = @"";
    NSString *targetString = [self.textField stringValue];
    NSDate *start = [NSDate date];
    JASGeneticAlgo *algo = [[JASGeneticAlgo alloc] initWithTargetSequence:targetString];
    algo.algoDelegate = self;
    [algo execute];
    NSTimeInterval runtime = [start timeIntervalSinceNow] * -1;
    NSString *msg = [NSString stringWithFormat:
                     @"\nOutput Sequence: %@\nElapsed Generations: %li\nDuration: %.2f seconds",
                     algo.result, 
                     algo.generations, 
                     runtime];
    [self performSelectorOnMainThread:@selector(updateTextViewWithText:) withObject:msg waitUntilDone:NO];
}

#pragma mark - JASGeneticAlgoDelegate

- (void)JASGeneticAlgoDidFindNewAlphaMaleWithGenes:(NSString *)genes
{
    [self performSelectorOnMainThread:@selector(updateTextFieldWithText:) withObject:genes waitUntilDone:NO];
    [self performSelectorOnMainThread:@selector(updateTextViewWithText:) withObject:genes waitUntilDone:NO];
}

- (void)updateTextFieldWithText:(NSString *)text
{
    self.textField.stringValue = text;
}

- (void)updateTextViewWithText:(NSString *)text;
{
    self.console = [self.console stringByAppendingString:[NSString stringWithFormat:@"%@\n", text]];
    [self.textView setString:self.console];
    [self.textView scrollRangeToVisible:NSMakeRange(self.textView.string.length - 1, 1)];
}

@end
