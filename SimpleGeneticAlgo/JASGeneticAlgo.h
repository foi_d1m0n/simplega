#import <Foundation/Foundation.h>

@class JASGeneticAlgo;

@protocol JASGeneticAlgoDelegate <NSObject>

- (void)JASGeneticAlgoDidFindNewAlphaMaleWithGenes:(NSString *)genes;

@end

@interface JASGeneticAlgo : NSObject

@property (nonatomic, readonly, assign) NSInteger generations;
@property (nonatomic, readonly, strong) NSString *result;

@property (nonatomic, weak) id <JASGeneticAlgoDelegate> algoDelegate;

- (id)initWithTargetSequence:(NSString *)sequence;

- (void)execute;

@end
