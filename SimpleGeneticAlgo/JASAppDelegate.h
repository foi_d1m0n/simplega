#import <Cocoa/Cocoa.h>

@interface JASAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (unsafe_unretained) IBOutlet NSTextView *textView;
@property (weak) IBOutlet NSTextField *textField;

- (IBAction)handleRunAlgorithmButton:(id)sender;

@end
