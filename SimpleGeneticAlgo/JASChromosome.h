#import <Foundation/Foundation.h>

@interface JASChromosome : NSObject

@property (nonatomic, readonly, strong) NSString *geneSequence;

- (id)initWithGeneCount:(NSUInteger)count;

- (JASChromosome *)mateWithChromosome:(JASChromosome *)other;

- (BOOL)isFitterThanChromosome:(JASChromosome *)other 
             forTargetSequence:(NSString *)sequence;

@end
